module.exports = {
    rest: {
      port: +(process.env.API_PORT || 8000),
      host: process.env.API_HOST || '0.0.0.0',
      openApiSpec: {
        setServersFromRequest: true,
      },
      listenOnStart: false,
  
      cors: {
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
        optionsSuccessStatus: 204,
        maxAge: 86400,
        credentials: true,
      }
  
    },
    configuration: {
      name: 'main',
      connector: 'mongodb',
      url: process.env.DB_URL,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      useNewUrlParser: true,
      allowExtendedOperators: true
    },
    services: {
        log: {
            enableMetadata: true
          },
          notifications: {
            email: {
              type: process.env.SERVICES_NOTIFICATIONS_EMAIL_TYPE || 'smtp',
              settings: {
                defaultFromEmail: process.env.SERVICES_NOTIFICATIONS_EMAIL_USER,
                service: process.env.SERVICES_NOTIFICATIONS_EMAIL_SERVICE || 'gmail',
                auth: {
                  user: process.env.SERVICES_NOTIFICATIONS_EMAIL_USER || 'thrizerusers@gmail.com',
                  pass: process.env.SERVICES_NOTIFICATIONS_EMAIL_PASSWORD || 'Thrizer123!',
                },
              }
            }
          },
    },
    procurement: {
        basePath: '/',
        test: 'TEST',
        email: {
          to: process.env.TEMPLATE_EMAIL_TO
        },
        db: {
          name: "ApiDS",
          connector: "memory",
          localStorage: "",
          file: ""
        }
      },
}