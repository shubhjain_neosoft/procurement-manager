import {Entity, model, property} from '@loopback/repository';

@model()
export class ProcurementManager extends Entity {
  @property({
    type: 'string',
  })
  firstName?: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  lastName?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'number',
  })
  type?: number;

  @property({
    type: 'date',
  })
  created?: string;

  @property({
    type: 'date',
  })
  updated?: string;

  [prop: string]: any;
  
  constructor(data?: Partial<ProcurementManager>) {
    super(data);
  }
}

export interface ProcurementManagerRelations {
  // describe navigational properties here
}

export type ProcurementManagerWithRelations = ProcurementManager & ProcurementManagerRelations;
