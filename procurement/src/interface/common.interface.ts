export interface DBConfiguration {
    name: string;
    connector: string;
    url: string;
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
    useNewUrlParser: boolean;
    allowExtendedOperators: boolean;
  }