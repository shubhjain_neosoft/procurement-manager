import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import {DBConfiguration} from '../interface/common.interface';
require('dotenv').config();
const config = require('config');

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html

@lifeCycleObserver('datasource')
export class MainDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'main';
  static readonly defaultConfig = config.configuration;

  constructor(
    @inject('datasources.config.main', {optional: true})
    dsConfig: DBConfiguration = config.configuration,
  ) {
    super(dsConfig);
  }
}
