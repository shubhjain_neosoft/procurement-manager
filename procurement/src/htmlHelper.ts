import * as fs from 'fs';
import * as handlebar from 'handlebars';

/* eslint-disable @typescript-eslint/no-explicit-any*/

export const TEMPLATER = {
  makeHtmlTemplate: async function (source: any, data: any) {
    return new Promise<string>((resolve, reject) => {
      fs.readFile(source, 'utf8', (err, content) => {
        if (err) {
          reject(err);
        }
        try {
          const template = handlebar.compile(content, {noEscape: true});
          const html = template(data);
          resolve(html);
        } catch (error) {
          reject(err);
        }
      });
    });
  },
};
